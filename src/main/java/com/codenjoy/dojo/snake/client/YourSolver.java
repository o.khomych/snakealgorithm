package com.codenjoy.dojo.snake.client;

/*-
 * #%L
 * Codenjoy - it's a dojo-like platform from developers to developers.
 * %%
 * Copyright (C) 2018 Codenjoy
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import com.codenjoy.dojo.client.Solver;
import com.codenjoy.dojo.client.WebSocketRunner;
import com.codenjoy.dojo.services.Dice;
import com.codenjoy.dojo.services.Direction;
import com.codenjoy.dojo.services.RandomDice;
import com.codenjoy.dojo.snake.algorithm.SnakeAlgorithm;

/**
 * User: your name
 */
public class YourSolver implements Solver<Board> {

    private Dice dice;
    private Board board;

    public YourSolver(Dice dice) {
        this.dice = dice;
    }

    @Override
    public String get(Board board) {
        try{
            this.board = board;
            SnakeAlgorithm snakeAlgorithm = new SnakeAlgorithm(board);
            return snakeAlgorithm.run();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return Direction.DOWN.toString();

        }

    }

    public static void main(String[] args) {
        String url = "http://64.226.126.93/codenjoy-contest/board/player/z2wii3i8m1dyrs9jjsdq?code=5762529237633303074";
        WebSocketRunner.runClient(url,
                new YourSolver(new RandomDice()),
                new Board());
    }

}
