package com.codenjoy.dojo.snake.algorithm;

import com.codenjoy.dojo.services.Point;

import java.util.LinkedList;
import java.util.ListIterator;

public class GameBoard {
    private final int[][] board;
    private final int BWIDTH = 15;
    private final int BHEIGHT = 15;
    private final int EMPTY = 0;
    private final int OBSTACLES = -9;
    private final int PATH = -1;
    private final int REJECTEDPATH = -10;
    private final Obstacles obstacles;
    public GameBoard(Obstacles obstacles) {
        this.board = new int[BWIDTH][BHEIGHT];
        this.obstacles = obstacles;
        setObstacles(obstacles);
    }
    private int get(int x, int y){
        return board[y][x];
    }
    int get(Point point){
        return get(point.getY(), point.getX());
    }
    private void set(int x, int y, int value){
        board[y][x] = value;
    }
    void set(Point point, int value){
        set(point.getY(), point.getX(), value);
    }

    boolean isOnBoard(Point point){
        return point.getX() > 0 && point.getX() < BWIDTH &&
                point.getY() > 0 && point.getY() < BHEIGHT;
    }

    public boolean isEmptyCell(Point point){
        return get(point) == EMPTY;
    }

    public boolean isSafeCell(Point point){
        return get(point) >= EMPTY;
    }
    public void setObstacles(Obstacles obstacles){
        obstacles.getObstacles().forEach(o -> set(o, OBSTACLES));
        setTailAsSafe();
    }
    private void setTailAsSafe(){
        Point tail = obstacles.getTail();
        set(tail, 0);
    }
    public void setPath(Point o){
        set(o, PATH);
    }
    public void setRejectedPath(Point o){
        set(o, REJECTEDPATH);
    }

    public ListIterator<Point> updateSnakeBody(){
        LinkedList<Point> snake = obstacles.snake().snake();
        return snake.listIterator(snake.size());
    }
    public void setStoneSafe(){
        obstacles.addStoneAsApple();
        set(obstacles.stones().get(0), 0);
    }
}
