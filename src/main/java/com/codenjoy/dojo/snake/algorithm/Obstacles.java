package com.codenjoy.dojo.snake.algorithm;

import com.codenjoy.dojo.services.Point;
import com.codenjoy.dojo.snake.client.Board;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public record Obstacles(Snake snake, Point head, List<Point> apples, List<Point> stones, List<Point> walls) {
    public static Obstacles of(Board board){
        Snake newSnake = new Snake(board);
        if (board.getSnake().isEmpty()) throw new NullPointerException("Snake is null");
        return new Obstacles(newSnake, newSnake.getHead(), board.getApples(), board.getStones(), board.getWalls());
    }

    public static Obstacles of(Snake newSnake, Board board){
        if (board.getSnake().isEmpty()) throw new NullPointerException("Snake is null");
        return new Obstacles(newSnake, newSnake.getHead(), board.getApples(), board.getStones(), board.getWalls());
    }
    public List<Point> getObstacles (){
        return Stream.of(snake.snake(), stones, walls)
                .flatMap(List<Point>::stream)
                .collect(Collectors.toList());
    }
    public Point getTail(){
        return snake.getTail();
    }
    public void addStoneAsApple(){
        apples.addAll(stones);
    }
    public int snakeSize(){
        return snake.snakeSize();
    }
}
