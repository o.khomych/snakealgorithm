package com.codenjoy.dojo.snake.algorithm;

import com.codenjoy.dojo.services.Point;
import com.codenjoy.dojo.snake.client.Board;
import com.codenjoy.dojo.snake.model.Elements;

import java.util.*;

import static com.codenjoy.dojo.snake.model.Elements.*;

public class Snake {
    private final LinkedList<Point> snake;
    private final Board board;
    public Snake(Board board){
        this.board = board;
        this.snake = sorted();
    }
    public Snake(LinkedList<Point> snake, Board board){
        this.snake = snake;
        this.board = board;
    }
    public LinkedList<Point> snake(){
        return snake;
    }
    public Point getTail(){
        List<Point> snake = board.getSnake();
        for(Point s: snake){
            if (board.isAt(s, TAIL_END_DOWN)) return s;
        }
        Optional<Point> first = snake.stream().filter(s -> tailIdentificators().stream().anyMatch(e -> board.isAt(s, e))).findFirst();
        return first.orElseThrow(()->new RuntimeException("Something go wrong with snake"));
    }
    public Point getHead(){
        return snake.get(0);
    }
    private List<Elements> tailIdentificators(){
        ArrayList<Elements> elements = new ArrayList<>();
        elements.add(TAIL_END_DOWN);
        elements.add(TAIL_END_LEFT);
        elements.add(TAIL_END_UP);
        elements.add(TAIL_END_RIGHT);
        return elements;
    }
    public int snakeSize(){
        return snake().size();
    }
    private List<Elements> headIdentificators(){
        ArrayList<Elements> elements = new ArrayList<>();
        elements.add(HEAD_DOWN);
        elements.add(HEAD_LEFT);
        elements.add(HEAD_RIGHT);
        elements.add(HEAD_UP);
        return elements;
    }

    public LinkedList<Point> sorted(){
        LinkedList<Point> boardS = new LinkedList<>(board.getSnake());
        LinkedList<Point> newS = new LinkedList<>();

        Point head = boardS.removeFirst();
        newS.add(head);
        Point tail = getTail();
        boardS.remove(tail);

        Point current = head;
        while (!boardS.isEmpty()){
            ListIterator<Point> iterator = boardS.listIterator();

            while (iterator.hasNext()) {
                Point p = iterator.next();
                if (areNeighbors(p, current)) {
                    newS.add(p);
                    current = p;
                    iterator.remove();
                    break;
                }
            }
        }
        newS.add(tail);
        return newS;
    }

    private boolean areNeighbors(Point borderPoint, Point snakeLastPoint){
        int xDiff = borderPoint.getX()-snakeLastPoint.getX();
        int yDiff = borderPoint.getY()-snakeLastPoint.getY();

        if (Math.abs(xDiff) + Math.abs(yDiff) != 1) return false;

        Elements typeOfElement = board.getAt(borderPoint);
        if (xDiff == 1 ) {
            return (typeOfElement.equals(TAIL_HORIZONTAL) || typeOfElement.equals(TAIL_LEFT_DOWN)||typeOfElement.equals(TAIL_LEFT_UP));
        }
        if (xDiff == -1 ) {
            return (typeOfElement.equals(TAIL_HORIZONTAL) || typeOfElement.equals(TAIL_RIGHT_DOWN)||typeOfElement.equals(TAIL_RIGHT_UP));
        }
        if (yDiff == -1 ) {
            return (typeOfElement.equals(TAIL_VERTICAL) || typeOfElement.equals(TAIL_RIGHT_UP)||typeOfElement.equals(TAIL_LEFT_UP));
        }
        if (yDiff == 1 ) {
            return typeOfElement.equals(TAIL_VERTICAL) || typeOfElement.equals(TAIL_RIGHT_DOWN) || typeOfElement.equals(TAIL_LEFT_DOWN);
        }
        return false;
    }
    public static Snake buildFutureSnake(Path futurePath, Snake snake, Board board){

        LinkedList<Point> newFuturePath = new LinkedList<>(futurePath.path());
        Collections.reverse(newFuturePath);
        newFuturePath.addAll(snake.snake());

        int i = snakeSizeCorrect(futurePath, board);
        LinkedList<Point> newSnakeList = new LinkedList<>();

        for (int j = 0; j < snake.snakeSize() + i; j++) {
            newSnakeList.add(newFuturePath.get(j));
        }

        return new Snake(newSnakeList, board);
    }

    private static int snakeSizeCorrect(Path path, Board board){
        Point last = path.getLast();
        List<Point> apples = board.getApples();
        return apples.indexOf(last) == 1 ? -10 : 1;
    }
}