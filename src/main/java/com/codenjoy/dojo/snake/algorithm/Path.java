package com.codenjoy.dojo.snake.algorithm;

import com.codenjoy.dojo.services.Point;

import java.util.LinkedList;

public record Path(LinkedList<Point> path) {
    public static Path of(){
        return  new Path(new LinkedList<>());
    }
    public Point getFirst(){
        return path.getFirst();
    }
    public Point getLast(){
        return path.getLast();
    }
    public void add(Point p){
        path.add(p);
    }
    public Point removeLast(){
        return path.removeLast();
    }
    public void addFirst(Point p){
        path.addFirst(p);
    }
    public int size(){
        return path.size();
    }

}
