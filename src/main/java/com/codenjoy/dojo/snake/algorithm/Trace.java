    package com.codenjoy.dojo.snake.algorithm;

    import com.codenjoy.dojo.services.Point;
    import com.codenjoy.dojo.services.PointImpl;
    import com.codenjoy.dojo.snake.client.Board;

    import java.util.*;
    import java.util.function.Supplier;
    import java.util.stream.Collectors;
    import java.util.stream.Stream;

    public class Trace {
        private final GameBoard gameBoard;
        private final Point START;
        private final Obstacles obstacles;
        private Trace(Obstacles obstacles) {
            this.obstacles = obstacles;
            this.gameBoard = new GameBoard(obstacles);
            this.START = obstacles.head();
        }
        public static Trace buildTrace(Obstacles obstacles){
            return new Trace(obstacles);
        }
        public static Trace getNewTrace(Board board){
            Obstacles obs = Obstacles.of(board);
            return new Trace(obs);
        }

        public void safeStone(){
            gameBoard.setStoneSafe();
        }

        private Supplier<Stream<Point>> deltas() {
            return () -> Stream.of(
                    new PointImpl(-1,0),
                    new PointImpl(0,1),
                    new PointImpl(1,0),
                    new PointImpl(0,-1)
            );
        }
        private Stream<Point> neighbours (Point point){
            return deltas().get()
                    .map(d -> {
                        Point point1 = new PointImpl(point);
                        point1.change(d);
                        return point1;
                    })
                    .filter(gameBoard::isOnBoard);
        }
        private Stream<Point> neighboursUnvisited (Point point) {
            return  neighbours(point)
                    .filter(gameBoard::isEmptyCell);
        }
        private List<Point> neighboursByValue(Point point, int value){
            return neighbours(point)
                    .filter(p -> gameBoard.get(p) == value)
                    .sorted(Comparator.comparingInt(this::avoidBoardCell))
                    .toList();
        }
        private Stream <Point> neighboursSafe(Point point) {
            return  neighbours(point)
                    .filter(gameBoard::isSafeCell);
        }
        
        private int avoidBoardCell(Point point){
           return isNearBorder(point) ? 1 : 0;
        }
        private boolean isNearBorder(Point point){
            return point.getX() == 1 || point.getY() == 1 || point.getX() == 13 || point.getY() == 13;
        }


        public Optional<Path> shortTrace(List<Point> finish) {
            int[] counter = new int[1];
            gameBoard.set(START, counter[0]);

            counter[0]++;
            boolean found = false;
            Optional<Point> target = Optional.empty();
                for(Set<Point> curr = new HashSet<>(Set.of(START)); !(found || curr.isEmpty() || finish.isEmpty()); counter[0]++){
                    Set<Point> next = curr.stream()
                            .flatMap(this::neighboursUnvisited)
                            .collect(Collectors.toSet());
                    next.forEach(p -> gameBoard.set(p, counter[0]));
                    Optional<Point> first = finish.stream().filter(next::contains).findFirst();
                    target = first;
                    curr = next;
                    if (first.isPresent()) {
                        found = true;
                    }
                }

            if(!found) return Optional.empty();
            return Optional.of(getTrace(target.get(), counter[0]));
        }


           private Path getTrace(Point finish, int counter){
                Path path = Path.of();
                path.add(finish);
                counter--;
                Point curr = finish;
                while (counter > 1){
                    counter--;
                    Point prev = neighboursByValue(curr, counter).get(0);
                    if(prev == START) break;
                    path.addFirst(prev);
                    curr = prev;
                }
                return path;
            }
        public Optional<Path> longerPaths(int length){
            Path newPath = Path.of();
            gameBoard.setObstacles(obstacles);
            List<Point> s = neighboursSafe(START).toList();

            ListIterator<Point> pointListIterator = gameBoard.updateSnakeBody();
            while (newPath.size() < length) {
                Optional<Point> targetPoint = s.stream()
                        .filter(point -> newPath.path().stream().noneMatch(point::equals))
                        .filter(p -> getFreeCellCounts(p) > length)
                        .min(Comparator.comparingInt(this::avoidBoardCell));

                if (targetPoint.isPresent()) {
                    newPath.add(targetPoint.get());
                    gameBoard.setPath(targetPoint.get());
                    if(pointListIterator.hasPrevious())  gameBoard.set(pointListIterator.previous(), 0);

                    s = neighboursSafe(targetPoint.get()).toList();

                } else {
                    if (newPath.size() <= 1) return Optional.empty();

                    Point last = newPath.removeLast();
                    gameBoard.setRejectedPath(last);

                    Point prev = newPath.getLast();
                    s = neighboursSafe(prev).toList();
                }
            }
            return Optional.of(newPath);
        }

        public Path longPath(){
            Path newPath = Path.of();
            gameBoard.setObstacles(obstacles);

            Optional<Point> targetPoint = neighboursSafe(START)
                    .filter(point -> newPath.path().stream().noneMatch(point::equals))
                    .sorted(Comparator.comparingInt(this::avoidBoardCell))
                    .max(Comparator.comparingInt(this::getFreeCellCounts));

            targetPoint.ifPresent(newPath::add);
            return newPath;
        }

        private void getCountRecursive(Point current, Set<Point> visited) {
            visited.add(current);

            List<Point> neighbors = neighboursSafe(current).toList();
            for (Point neighbor : neighbors) {
                if (!visited.contains(neighbor)) {
                    getCountRecursive(neighbor, visited);
                }
            }
        }
        private Set<Point> getCount(Point point) {
            Set<Point> visited = new HashSet<>();
            getCountRecursive(point, visited);
            return visited;
        }
        public int getFreeCellCounts(){
            return getCount(START).size()-1;
        }
        public int getFreeCellCounts(Point point){
            return getCount(point).size();
        }
        public Point getSafeCell(){
            List<Point> nextPoints = neighboursSafe(obstacles.snake().getHead())
                    .sorted(Comparator.comparingInt(this::getFreeCellCounts)).toList();
            int i = (int) (Math.random() * nextPoints.size());
            if (nextPoints.isEmpty()) throw new ArrayIndexOutOfBoundsException();
            return nextPoints.get(i);
        }

    }
