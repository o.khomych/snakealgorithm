package com.codenjoy.dojo.snake.algorithm;

import com.codenjoy.dojo.services.Direction;
import com.codenjoy.dojo.services.Point;
import com.codenjoy.dojo.snake.client.Board;

import java.util.Optional;

public class SnakeAlgorithm {
    private final Board board;
    private final Obstacles obstacles;
    private final Trace trace;
    public SnakeAlgorithm(Board board) {
        this.board = board;
        this.obstacles = Obstacles.of(board);
        this.trace = Trace.buildTrace(obstacles);
    }
    public String run() {
        try{
            return moveDirection(moveTo()).toString();
        } catch(Exception e){
            System.out.println(e.getMessage());
          return randomMove();
        }
    }
    private Direction moveDirection(Point curr){
        Point headAt = board.getHead();

        if (curr.getY() < headAt.getY()) return Direction.DOWN;
        if (curr.getX() > headAt.getX()) return Direction.RIGHT;
        if (curr.getY() > headAt.getY()) return Direction.UP;
        if (curr.getX() < headAt.getX()) return Direction.LEFT;
        return Direction.DOWN;
    }
    public Point moveTo(){
        return getFullPath().getFirst();
    }
    private Path getFullPath(){
        if (obstacles.snakeSize() >= 55) trace.safeStone();

        Optional<Path> shortTrace = trace.shortTrace(obstacles.apples());

        if (shortTrace.isPresent()){
            return safeShortPath(shortTrace.get(), trace);
        }
        return alternitivePath();

    }

    private Path safeShortPath(Path shortT, Trace trace){
        Optional<Path> path = safePath(shortT, trace);
        return path.orElseGet(this::alternitivePath);
    }

    private Optional<Path> safePath(Path shortT, Trace trace){
        if(isSafeTrace(shortT))     return Optional.of(shortT);

        Optional<Path> longPath = safeLongPath(trace, obstacles.snakeSize());
        if (longPath.isPresent())   return longPath;


        Trace newTrace = Trace.getNewTrace(board);
        return safeLongPath(newTrace, obstacles.snakeSize()/2);
    }
    private Path alternitivePath(){
        Optional<Path> stonePath = eatStonePath();
        if (stonePath.isPresent())   return stonePath.get();

        Optional<Path> longPath = safeLongPath(trace, obstacles.snakeSize());
        if (longPath.isPresent())    return longPath.get();

        Trace newTrace = Trace.getNewTrace(board);
        newTrace.safeStone();

        return newTrace.longPath();

    }
    private Optional<Path> eatStonePath(){
        Obstacles obs = Obstacles.of(board);
        Trace newTrace = Trace.getNewTrace(board);
        newTrace.safeStone();

        Optional<Path> shortTrace = newTrace.shortTrace(obs.apples());
        if (shortTrace.isPresent()) return safePath(shortTrace.get(), newTrace);
        return Optional.empty();
    }

    private Optional<Path> safeLongPath(Trace trace, int len){
        Optional<Path> longPath = trace.longerPaths(len);
        return  (longPath.isPresent() && isSafeTrace(longPath.get())) ? longPath : Optional.empty();
    }

    private String randomMove(){
        Trace newTrace = Trace.getNewTrace(board);
        return moveDirection(newTrace.getSafeCell()).toString();
    }

    private boolean isSafeTrace(Path futurePath){
        Snake snake = Snake.buildFutureSnake(futurePath, obstacles.snake(), board);
        Obstacles obs = Obstacles.of(snake, board);
        Trace futureTrace = Trace.buildTrace(obs);
        int allSafeCells = futureTrace.getFreeCellCounts();

        return allSafeCells >= snake.snakeSize()+1;
    }


}
